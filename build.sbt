val projectScalaVersion = "2.12.13"

lazy val root = (project in file(".")).
  settings(
    organization := "com.availity",
    name := "provider",
    version := "1.0",
    scalaVersion := projectScalaVersion,
    mainClass in Compile := Some("com.availity.spark.provider.ProviderRoster")
  )

val sparkVersion = "3.1.1"
//val sparkVersion = "3.5.1"

libraryDependencies ++= Seq(
    "org.apache.spark" %% "spark-core" % sparkVersion % "provided",
    "org.apache.spark" %% "spark-sql" % sparkVersion % "provided",
    // swap in the below deps for 'sbt run'
    // "org.apache.spark" %% "spark-core" % sparkVersion,
    // "org.apache.spark" %% "spark-sql" % sparkVersion,
    "com.github.mrpowers" %% "spark-daria" % "1.2.3",
    "com.github.mrpowers" %% "spark-fast-tests" % "1.3.0" % Test,
    "org.scalatest" %% "scalatest" % "3.2.9" % Test
)

assembly / assemblyJarName := s"${name.value}.jar"
// META-INF discarding
assembly / assemblyMergeStrategy := {
    case PathList("META-INF", xs @ _*) => MergeStrategy.discard
    case x => MergeStrategy.first
}
assembly / assemblyOption ~= {
  _.withIncludeScala(false)
}
assembly / test := {}
// uncomment for 'sbt run'
// run / fork := true

