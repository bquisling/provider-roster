package com.availity.spark.provider

import org.apache.spark.sql.{SparkSession, DataFrame}
import org.apache.spark.sql.types.{DateType, IntegerType, StructField, StructType}
import org.apache.spark.sql.functions.{col, collect_list, count, date_format, struct}

import java.nio.file.{Paths, Files}
import java.nio.charset.StandardCharsets

object ProviderRoster {

  def main(args: Array[String]): Unit = {
    process()
  }

  def process(): Unit = {
    println("Processing the provider roster...")

    val spark = SparkSession.builder()
      .appName("Provider Visits")
      .master("local[*]")
      .getOrCreate()

    // Define schema for the visits data
    val visitsSchema = StructType(Array(
      StructField("visit_id", IntegerType, nullable = true),
      StructField("provider_id", IntegerType, nullable = true),
      StructField("date_of_service", DateType, nullable = true)
    ))

    // Load the providers and visits data
    val providers = spark.read
      .option("header", "true")
      .option("delimiter", "|")
      .csv("data/providers.csv")

    val visits = spark.read
      .schema(visitsSchema)
      .option("header", "false")
      .csv("data/visits.csv")

    val result1 = calculateTotalVisitsPerProvider(providers, visits)
    val result2 = calculateTotalVisitsPerProviderPerMonth(visits)

    writeResultToFile(result1, "output/total_visits_per_provider.json")
    writeResultToFile(result2, "output/total_visits_per_provider_per_month.json")

    spark.stop()
  }

  def calculateTotalVisitsPerProvider(providers: DataFrame, visits: DataFrame): String = {
    // Total number of visits per provider
    val visitsPerProvider = visits.groupBy("provider_id")
      .agg(count("visit_id").as("total_visits"))

    val providerDetailsWithVisits = providers.join(visitsPerProvider, "provider_id")

    providerDetailsWithVisits
      .select("provider_id", "first_name", "middle_name", "last_name", "provider_specialty", "total_visits")
      .groupBy("provider_specialty")
      .agg(collect_list(
        struct("provider_id", "first_name", "middle_name", "last_name", "total_visits")
      ).as("providers"))
      .toJSON
      .collect()
      .mkString("[", ",", "]")
  }

  def calculateTotalVisitsPerProviderPerMonth(visits: DataFrame): String = {
    // Total number of visits per provider per month
    val visitsPerMonth = visits
      .withColumn("month", date_format(col("date_of_service"), "yyyy-MM"))
      .groupBy("provider_id", "month")
      .agg(count("visit_id").as("total_visits"))

    visitsPerMonth.toJSON
      .collect()
      .mkString("[", ",", "]")
  }

  def writeResultToFile(result: String, path: String): Unit = {
    val outputDir = Paths.get("output")
    if (!Files.exists(outputDir)) {
      Files.createDirectory(outputDir)
    }
    Files.write(Paths.get(path), result.getBytes(StandardCharsets.UTF_8))
  }
}
