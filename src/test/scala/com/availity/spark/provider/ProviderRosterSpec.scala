package com.availity.spark.provider

import com.github.mrpowers.spark.fast.tests.DataFrameComparer
import org.scalatest.BeforeAndAfterEach
import org.scalatest.funspec.AnyFunSpec
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import java.nio.file.{Files, Paths}
import java.nio.charset.StandardCharsets

class ProviderRosterSpec extends AnyFunSpec with DataFrameComparer with SparkSessionTestWrapper {

  import spark.implicits._

  describe("ProviderRoster") {

    it("should calculate total visits per provider") {

      var providers2 = Seq(
        (47259, "Urology", "Bessie", "B", "Kuphal"),
        (17847, "Cardiology", "Candice", "C", "Block")
      )

      var providers = Seq(
        (47259, "Urology", "Bessie", "B", "Kuphal"),
        (17847, "Cardiology", "Candice", "C", "Block")
      ).toDF("provider_id", "provider_specialty", "first_name", "middle_name", "last_name")

      val visits = Seq(
        (40838, 47259, "2022-08-23"),
        (58362, 47259, "2022-03-25"),
        (65989, 47259, "2021-10-28")
      ).toDF("visit_id", "provider_id", "date_of_service")
        .withColumn("date_of_service", col("date_of_service").cast("date"))

      val result = ProviderRoster.calculateTotalVisitsPerProvider(providers, visits)
      val expectedJson = """[{"provider_specialty":"Urology","providers":[{"provider_id":47259,"first_name":"Bessie","middle_name":"B","last_name":"Kuphal","total_visits":3}]}]"""
      
      assert(result == expectedJson)
    }

    it("should calculate total visits per provider per month") {

      val visits = Seq(
        (65989, 47259, "2021-10-28"),
        (58362, 47259, "2022-03-25"),
        (40838, 47259, "2022-08-23")
      ).toDF("visit_id", "provider_id", "date_of_service")
        .withColumn("date_of_service", col("date_of_service").cast("date"))

      val result = ProviderRoster.calculateTotalVisitsPerProviderPerMonth(visits)

      val expectedJson = """[{"provider_id":47259,"month":"2021-10","total_visits":1},{"provider_id":47259,"month":"2022-08","total_visits":1},{"provider_id":47259,"month":"2022-03","total_visits":1}]"""

      assert(result == expectedJson)
    }

    it("should write result to file") {
      val result = """[{"provider_id":47259,"month":"2021-10","total_visits":1},{"provider_id":47259,"month":"2022-08","total_visits":1},{"provider_id":47259,"month":"2022-03","total_visits":1}]"""
      val path = "output/test_total_visits_per_provider.json"

      ProviderRoster.writeResultToFile(result, path)

      val writtenContent = new String(Files.readAllBytes(Paths.get(path)), StandardCharsets.UTF_8)
      assert(writtenContent == result)

      // Clean up the test file
      Files.delete(Paths.get(path))
    }
  }
}
